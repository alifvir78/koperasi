package com.koperasi.controller;

import com.koperasi.dto.simpanan.SimpananRequest;
import com.koperasi.dto.simpanan.SimpananRespone;
import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.service.SimpananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@CrossOrigin(origins = "*")


@RestController
@RequestMapping("/koperasi/simpanan")
public class SimpananController {

    @Autowired
    private SimpananService simpananService;


    @GetMapping
    public ResponseEntity<List<SimpananRespone>> getAllSimpanan(@RequestHeader("token") String token) {
        List<SimpananRespone> response = simpananService.getAllSimpanan(token);
        return ResponseEntity.ok(response);
    }


    @PostMapping("/add")
    public ResponseEntity<SimpananRespone> saveBarang(@RequestHeader String token, @RequestBody SimpananRequest request)  {
        SimpananRespone respone = simpananService.saveSimpanan(token,request);
        return ResponseEntity.ok(respone);
    }

    @PutMapping("/{id}")
    public  ResponseEntity<SimpananRespone> updateSimpananById(@RequestHeader("token")String token, @PathVariable("id")Integer id, @RequestBody SimpananRequest request) throws TokenInvalidException {
        SimpananRespone respone = simpananService.updateSimpananById(token,id, request);
        return ResponseEntity.ok(respone);
    }



    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSimpananById(@RequestHeader("token")String token,@PathVariable("id") Integer id) throws TokenInvalidException {
        String respone = simpananService.deleteSimpananById(token,id);
        return ResponseEntity.ok(respone);
    }







}
