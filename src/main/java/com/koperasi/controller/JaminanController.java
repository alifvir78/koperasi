package com.koperasi.controller;

import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.dto.jaminan.JaminanRequest;
import com.koperasi.dto.jaminan.JaminanRespone;
import com.koperasi.service.JaminanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")


@RestController
@RequestMapping("/koperasi/jaminan")
public class JaminanController {

    @Autowired
    private JaminanService jaminanService;


    @GetMapping
        public ResponseEntity<List<JaminanRespone>> getAllJaminan(@RequestHeader("token") String token) throws TokenInvalidException {
        List<JaminanRespone> response = jaminanService.getAllJaminan(token);
        return ResponseEntity.ok(response);
    }


    @PostMapping("/add")
    public ResponseEntity<JaminanRespone> saveJaminan(@RequestHeader String token, @RequestBody JaminanRequest request) {
        JaminanRespone respone = jaminanService.saveJaminan(token,request);
        return ResponseEntity.ok(respone);
    }

    @PutMapping("/{id}")
    public  ResponseEntity<JaminanRespone> updateJaminanById(@RequestHeader("token")String token, @PathVariable("id")Integer id, @RequestBody JaminanRequest request)  {
        JaminanRespone respone = jaminanService.updateJaminanById(token,id, request);
        return ResponseEntity.ok(respone);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteJaminanById(@RequestHeader("token")String token,@PathVariable("id") Integer id) {
        String respone = jaminanService.deleteJaminanById(token,id);
        return ResponseEntity.ok(respone);
    }

}
