package com.koperasi.controller;

import com.koperasi.dto.pinjaman.DetailPinjaman;
import com.koperasi.dto.pinjaman.PinjamanRequest;
import com.koperasi.dto.pinjaman.PinjamanRespone;
import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.service.PinjamanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")


@RestController
@RequestMapping("/koperasi/pinjaman")
public class PinjamanController {

    @Autowired
    private PinjamanService pinjamanService;


    @GetMapping
    public ResponseEntity<List<PinjamanRespone>> getAllPinjaman(@RequestHeader("token") String token) {
        List<PinjamanRespone> response = pinjamanService.getAllPinjaman(token);
        return ResponseEntity.ok(response);
    }


    @GetMapping("/{id}/detail")
    public ResponseEntity<DetailPinjaman> getPinjamanDetail(@RequestHeader("token") String token, @PathVariable("id") Integer anggotaId) {
        DetailPinjaman response = pinjamanService.getPinjamanByAnggotaId(token, anggotaId);
        return ResponseEntity.ok(response);
    }


    @PostMapping("/add")
    public ResponseEntity<PinjamanRespone> savePinjaman(@RequestHeader String token, @RequestBody PinjamanRequest request) throws TokenInvalidException {
        PinjamanRespone respone = pinjamanService.savePinjaman(token,request);
        return ResponseEntity.ok(respone);
    }

    @PutMapping("/{id}")
    public  ResponseEntity<PinjamanRespone> updatePinjamanById(@RequestHeader("token")String token, @PathVariable("id")Integer id, @RequestBody PinjamanRequest request) throws TokenInvalidException {
        PinjamanRespone respone = pinjamanService.updatePinjamanById(token,id, request);
        return ResponseEntity.ok(respone);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePinjamanById(@RequestHeader("token")String token,@PathVariable("id") Integer id)  {
        String respone = pinjamanService.deletePinjamanById(token,id);
        return ResponseEntity.ok(respone);
    }


}
