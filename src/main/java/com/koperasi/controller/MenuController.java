package com.koperasi.controller;


import com.koperasi.dto.menu.MenuRequest;
import com.koperasi.dto.menu.MenuRespon;
import com.koperasi.entitiy.Menu;
import com.koperasi.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")

@RestController
@RequestMapping("/koperasi/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;


    @GetMapping
    public ResponseEntity <List<Menu>>getAllMenu(){
        List<Menu> response = menuService.getAllMenu();
        return ResponseEntity.ok(response);
    }

    @PostMapping("/add")
    public MenuRespon createMenu(@RequestBody MenuRequest request) {
        return menuService.createMenu(request);
    }


    @PutMapping("/{id}")
    public  ResponseEntity<MenuRespon> updateMenuById( @PathVariable("id")Integer id, @RequestBody MenuRequest request)  {
        MenuRespon respone = menuService.updateMenuById(id,request);
        return ResponseEntity.ok(respone);
    }
}
