package com.koperasi.controller;

import com.koperasi.dto.anggota.AnggotaRequest;
import com.koperasi.dto.anggota.AnggotaRespone;
import com.koperasi.exception.anggota.AnggotaDeletionException;
import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.service.AnggotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/koperasi/anggota")
public class AnggotaController {

    @Autowired
    private AnggotaService anggotaService;


    @GetMapping
    public ResponseEntity<List<AnggotaRespone>> getAllAnggota(@RequestHeader("token") String token) throws TokenInvalidException {
        List<AnggotaRespone> response = anggotaService.getAllAnggota(token);
        return ResponseEntity.ok(response); //200 ok
    }


    @PostMapping("/registrasi")  //@Valid dipake untuk melakukan validasi not blank
    public ResponseEntity<AnggotaRespone> saveAnggota(@Valid @RequestBody AnggotaRequest request){
        AnggotaRespone respone = anggotaService.saveAnggota(request);
        return new ResponseEntity<>(respone, HttpStatus.CREATED); //ini untuk melakukan 201, kalau respone ok 200
    }

    @PutMapping("/{id}")
    public  ResponseEntity<AnggotaRespone> updateAnggotaById(@RequestHeader("token")String token,@PathVariable("id")Integer id, @RequestBody AnggotaRequest request) throws TokenInvalidException {
        AnggotaRespone respone = anggotaService.updateAnggotaById(token,id, request);
        return ResponseEntity.ok(respone);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAnggotaById(@RequestHeader("token")String token,@PathVariable("id") Integer id) throws  AnggotaDeletionException {
        String respone = anggotaService.deleteAnggotaById(token,id);
        return ResponseEntity.ok(respone);
    }



}
