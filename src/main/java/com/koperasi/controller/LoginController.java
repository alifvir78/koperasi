package com.koperasi.controller;

import com.koperasi.dto.login.LoginRequest;
import com.koperasi.dto.login.LoginRespone;
import com.koperasi.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/koperasi/login")
public class LoginController {


    @Autowired
    private LoginService loginService;

    @PostMapping
    public ResponseEntity<LoginRespone> login(@RequestBody LoginRequest request){
        LoginRespone respone = loginService.login(request);
        return new ResponseEntity<>(respone, HttpStatus.OK);
    }

}
