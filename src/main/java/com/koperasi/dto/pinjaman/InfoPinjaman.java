package com.koperasi.dto.pinjaman;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class InfoPinjaman {

    private Integer pinjamanId;
    private String kodeTransaksi;
    private String keperluan;
    private String lamaAngsuran;
    private Double jumlahPinjaman;
    private BigDecimal bunga;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date tanggalTransaksi;

    private String detailJaminan;
    private Double nominalJaminan;



}
