package com.koperasi.dto.pinjaman;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DetailPinjaman {

    private String nama;
    private String alamat;
    private String noTelepon;
    private String email;
    private List<InfoPinjaman> pinjamanList;

}
