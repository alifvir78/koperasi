package com.koperasi.dto.pinjaman;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Builder
@Data
@AllArgsConstructor
public class PinjamanRespone {


    private Integer anggotaId;
    private Integer jaminanId;

    private String kodeTransaksi;
    private String keperluan;
    private String lamaAngsuran;
    private Double jumlahPinjaman;
    private BigDecimal bunga;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date tanggalTransaksi;

}
