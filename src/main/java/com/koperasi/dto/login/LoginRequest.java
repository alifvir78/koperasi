package com.koperasi.dto.login;

import lombok.Data;

@Data
public class LoginRequest {

    private String noTelepon;
    private String email;
}
