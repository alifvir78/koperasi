package com.koperasi.dto.simpanan;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class SimpananRespone {

    private Integer anggotaId;
    private Double jumlahSimpanan;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date tanggalSimpanan;

}
