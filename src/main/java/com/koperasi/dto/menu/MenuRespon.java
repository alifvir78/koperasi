package com.koperasi.dto.menu;

import lombok.Data;

import java.math.BigDecimal;

@Data
//@Builder
//@AllArgsConstructor
public class MenuRespon {

    private Integer id;
    private String nameMenu;
    private BigDecimal price;
    private String description;
    private String urlImage;
}
