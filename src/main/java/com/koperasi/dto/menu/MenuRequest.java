package com.koperasi.dto.menu;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MenuRequest {

    private String nameMenu;
    private BigDecimal price;
    private String description;
    private String urlImage;

}
