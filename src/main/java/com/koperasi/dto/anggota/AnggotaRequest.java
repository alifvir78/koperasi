package com.koperasi.dto.anggota;

import lombok.*;

import javax.validation.constraints.NotBlank;

//@Data
//@Builder
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnggotaRequest {


    @NotBlank(message = "Silahkan Masukan nama")
    private String nama;
    @NotBlank(message = "Silahkan Masukan alamat")
    private String alamat;
    @NotBlank(message = "Silahkan Masukan tanggalLahir")
    private String tanggalLahir;
    @NotBlank(message = "Silahkan Masukan jenisKelamin")
    private String jenisKelamin;
    @NotBlank(message = "Silahkan Masukan pekerjaan")
    private String pekerjaan;
    @NotBlank(message = "Silahkan Masukan NoTelepon")
    private String noTelepon;
    @NotBlank(message = "Silahkan Masukan email")
    private String email;


}
