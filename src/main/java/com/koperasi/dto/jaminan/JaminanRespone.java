package com.koperasi.dto.jaminan;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class JaminanRespone    {

    private Integer anggotaId;
    private String detailJaminan;
    private Double nominalJaminan;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date tanggalJaminan;

}
