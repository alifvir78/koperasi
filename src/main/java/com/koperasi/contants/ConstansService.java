package com.koperasi.contants;

public final class ConstansService {

    private ConstansService() {
    }

    public static final String TOKEN_INVALID_MESSAGE = "Token tidak valid";
    public static final String TIDAK_DITEMUKAN_MESSAGE = "Tidak ditemukan";
}

