package com.koperasi.repository;

import com.koperasi.entitiy.Jaminan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JaminanRepository extends JpaRepository<Jaminan,Integer> {
    List<Jaminan> findByAnggotaId(Integer id);
}
