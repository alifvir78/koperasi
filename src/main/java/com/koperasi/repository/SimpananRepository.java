package com.koperasi.repository;

import com.koperasi.entitiy.Simpanan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimpananRepository extends JpaRepository<Simpanan,Integer> {

    List<Simpanan> findByAnggotaId(Integer id);
}
