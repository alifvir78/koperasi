package com.koperasi.repository;

import com.koperasi.dto.pinjaman.InfoPinjaman;
import com.koperasi.entitiy.Pinjaman;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PinjamanRepository extends JpaRepository<Pinjaman,Integer> {


    @Query("SELECT new com.koperasi.dto.pinjaman.InfoPinjaman(p.id, p.kodeTransaksi, p.keperluan, p.lamaAngsuran, p.jumlahPinjaman, p.bunga, p.tanggalTransaksi, j.detailJaminan, j.nominalJaminan) FROM Pinjaman p JOIN p.jaminan j JOIN p.anggota a WHERE a.id=:id")
    List<InfoPinjaman> findDetailPinjaman(@Param("id") Integer id);


    List<Pinjaman> findByJaminanId(Integer id);

    List<Pinjaman> findByAnggotaId(Integer id);
}
