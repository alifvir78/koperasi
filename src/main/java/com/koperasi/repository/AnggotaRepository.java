package com.koperasi.repository;

import com.koperasi.entitiy.Anggota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnggotaRepository extends JpaRepository<Anggota,Integer> {

    @Query("SELECT a FROM Anggota a WHERE a.noTelepon = :noTelepon AND a.email = :email")
    Optional<Anggota> findWithUserDataByEmailAndNoTelepon(
            @Param("noTelepon") String noTelepon,
            @Param("email") String email
            );



}
