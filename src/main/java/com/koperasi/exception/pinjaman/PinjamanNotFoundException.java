package com.koperasi.exception.pinjaman;

public class PinjamanNotFoundException extends RuntimeException{

    public PinjamanNotFoundException(String message) {
        super(message);
    }

}
