package com.koperasi.exception.pinjaman;

public class PinjamanDeletionException extends RuntimeException{


    public PinjamanDeletionException(String message, Throwable cause) {
        super(message, cause);
    }
}
