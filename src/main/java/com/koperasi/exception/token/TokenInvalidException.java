package com.koperasi.exception.token;

public class TokenInvalidException extends Exception{
    public TokenInvalidException(String message) {
        super(message);
    }

    public TokenInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
}
