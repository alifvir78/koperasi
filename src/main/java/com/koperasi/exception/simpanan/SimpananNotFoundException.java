package com.koperasi.exception.simpanan;

public class SimpananNotFoundException extends RuntimeException{

    public SimpananNotFoundException(String message) {
        super(message);
    }

}
