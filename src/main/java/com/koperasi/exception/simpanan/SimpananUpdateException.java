package com.koperasi.exception.simpanan;

public class SimpananUpdateException extends RuntimeException{
    public SimpananUpdateException(String message) {
        super(message);
    }

}
