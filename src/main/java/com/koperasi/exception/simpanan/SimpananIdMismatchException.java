package com.koperasi.exception.simpanan;

public class SimpananIdMismatchException extends RuntimeException{
    public SimpananIdMismatchException(String message) {
        super(message);
    }

}
