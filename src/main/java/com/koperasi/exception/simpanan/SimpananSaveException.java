package com.koperasi.exception.simpanan;

public class SimpananSaveException extends RuntimeException{

    public SimpananSaveException(String message, Throwable cause) {
        super(message, cause);
    }
}
