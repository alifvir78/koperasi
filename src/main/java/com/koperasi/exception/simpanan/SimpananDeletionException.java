package com.koperasi.exception.simpanan;

public class SimpananDeletionException extends RuntimeException {


    public SimpananDeletionException(String message, Throwable cause) {
        super(message, cause);
    }
}
