package com.koperasi.exception.jaminan;

public class JaminanIdMismatchException extends RuntimeException{
    public JaminanIdMismatchException(String message) {
        super(message);
    }

}
