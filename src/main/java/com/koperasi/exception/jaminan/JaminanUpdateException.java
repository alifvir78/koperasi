package com.koperasi.exception.jaminan;

public class JaminanUpdateException extends RuntimeException{


    public JaminanUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

}
