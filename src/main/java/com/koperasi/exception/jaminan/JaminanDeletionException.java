package com.koperasi.exception.jaminan;

public class JaminanDeletionException extends RuntimeException{


    public JaminanDeletionException(String message, Throwable cause) {
        super(message, cause);
    }
}
