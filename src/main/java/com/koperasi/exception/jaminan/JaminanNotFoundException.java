package com.koperasi.exception.jaminan;

public class JaminanNotFoundException extends RuntimeException{

    public JaminanNotFoundException(String message) {
        super(message);
    }

}
