package com.koperasi.exception.jaminan;

public class JaminanSaveException extends RuntimeException{



    public JaminanSaveException(String message, Throwable cause) {
        super(message, cause);
    }
}
