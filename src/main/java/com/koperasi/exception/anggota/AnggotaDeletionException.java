package com.koperasi.exception.anggota;

public class AnggotaDeletionException extends Exception {


    public AnggotaDeletionException(String message, Throwable cause) {
        super(message, cause);
    }
}
