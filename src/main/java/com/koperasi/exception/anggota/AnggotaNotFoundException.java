package com.koperasi.exception.anggota;

public class AnggotaNotFoundException extends RuntimeException{

    public AnggotaNotFoundException(String message) {
        super(message);
    }

}
