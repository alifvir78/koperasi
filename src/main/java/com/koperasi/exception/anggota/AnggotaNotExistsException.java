package com.koperasi.exception.anggota;

public class AnggotaNotExistsException extends RuntimeException {

    public AnggotaNotExistsException(String message) {
        super(message);
    }

}
