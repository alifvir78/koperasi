package com.koperasi.exception.anggota;

public class DataRetrievalException extends RuntimeException{

    public DataRetrievalException(String message, Throwable cause) {
        super(message, cause);
    }
}
