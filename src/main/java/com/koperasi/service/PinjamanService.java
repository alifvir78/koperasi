package com.koperasi.service;

import com.koperasi.contants.*;
import com.koperasi.dto.pinjaman.DetailPinjaman;
import com.koperasi.dto.pinjaman.InfoPinjaman;
import com.koperasi.dto.pinjaman.PinjamanRequest;
import com.koperasi.dto.pinjaman.PinjamanRespone;
import com.koperasi.entitiy.Anggota;
import com.koperasi.entitiy.Jaminan;
import com.koperasi.entitiy.Pinjaman;
import com.koperasi.exception.anggota.AnggotaNotFoundException;
import com.koperasi.exception.jaminan.JaminanIdMismatchException;
import com.koperasi.exception.jaminan.JaminanNotFoundException;
import com.koperasi.exception.pinjaman.PinjamanDeletionException;
import com.koperasi.exception.pinjaman.PinjamanNotFoundException;
import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.repository.AnggotaRepository;
import com.koperasi.repository.JaminanRepository;
import com.koperasi.repository.PinjamanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PinjamanService {

    @Autowired
    private PinjamanRepository pinjamanRepository;

    @Autowired
    private JaminanRepository jaminanRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AnggotaRepository anggotaRepository;


    public List<PinjamanRespone> getAllPinjaman(String token) {
        if (!tokenService.getToken(token)) {
            return Collections.emptyList();
        }

        return pinjamanRepository.findAll().stream()
                .map(pinjaman -> PinjamanRespone.builder()
                        .anggotaId(pinjaman.getAnggota().getId())
                        .jaminanId(pinjaman.getJaminan().getId())
                        .kodeTransaksi(pinjaman.getKodeTransaksi())
                        .keperluan(pinjaman.getKeperluan())
                        .lamaAngsuran(pinjaman.getLamaAngsuran())
                        .jumlahPinjaman(pinjaman.getJumlahPinjaman())
                        .bunga(pinjaman.getBunga())
                        .tanggalTransaksi(new Date())
                        .build())
                .collect(Collectors.toList());
    }




    public DetailPinjaman getPinjamanByAnggotaId(String token,Integer anggotaId){
        if (!tokenService.getToken(token)) {
            return null;
        }

        Optional<Anggota> optionalAnggota = anggotaRepository.findById(anggotaId);
        if(!optionalAnggota.isPresent()){
            return null;
        }

        Anggota anggota = optionalAnggota.get();
        List<InfoPinjaman> pinjamanList = pinjamanRepository.findDetailPinjaman(anggotaId);
        return DetailPinjaman.builder()
                .nama(anggota.getNama())
                .alamat(anggota.getAlamat())
                .noTelepon(anggota.getNoTelepon())
                .email(anggota.getEmail())
                .pinjamanList(pinjamanList)
                .build();

    }



    public PinjamanRespone savePinjaman(String token, PinjamanRequest request) throws TokenInvalidException {
        if (!tokenService.getToken(token)) {
            throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
        }

        Pinjaman pinjaman = new Pinjaman();
        Anggota anggota = anggotaRepository.findById(request.getAnggotaId()).orElseThrow(() -> new EntityNotFoundException("Barang dengan ID " + request.getAnggotaId() + ConstansService.TIDAK_DITEMUKAN_MESSAGE));
        Jaminan jaminan = jaminanRepository.findById(request.getJaminanId()).orElseThrow(() -> new EntityNotFoundException("Pembeli dengan ID " + request.getJaminanId() + ConstansService.TIDAK_DITEMUKAN_MESSAGE));

        pinjaman.setTanggalTransaksi(new Date());
        pinjaman.setJumlahPinjaman(request.getJumlahPinjaman());
        pinjaman.setKeperluan(request.getKeperluan());
        pinjaman.setLamaAngsuran(request.getLamaAngsuran());
        pinjaman.setBunga(request.getBunga());
        pinjaman.setKodeTransaksi(UUID.randomUUID().toString().substring(0, 5).toUpperCase());
        pinjaman.setAnggota(anggota);
        pinjaman.setJaminan(jaminan);
        pinjamanRepository.save(pinjaman);

        return PinjamanRespone.builder()
                .anggotaId(pinjaman.getAnggota().getId())
                .jaminanId(pinjaman.getJaminan().getId())
                .kodeTransaksi(pinjaman.getKodeTransaksi())
                .keperluan(pinjaman.getKeperluan())
                .lamaAngsuran(pinjaman.getLamaAngsuran())
                .jumlahPinjaman(pinjaman.getJumlahPinjaman())
                .bunga(pinjaman.getBunga())
                .tanggalTransaksi(new Date())
                .build();
    }


    public PinjamanRespone updatePinjamanById(String token, Integer id, PinjamanRequest request) throws TokenInvalidException {
        if (!tokenService.getToken(token)) {
            throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
        }

        Optional<Pinjaman> optionalPinjaman = pinjamanRepository.findById(id);

        if (optionalPinjaman.isEmpty()) {
            throw new PinjamanNotFoundException("Pinjaman dengan ID " + id + ConstansService.TIDAK_DITEMUKAN_MESSAGE);
        }

        Pinjaman existingPinjaman = optionalPinjaman.get();

        // Cek apakah ID yang diminta sesuai dengan ID yang ditemukan
        if (!existingPinjaman.getId().equals(id)) {
            throw new JaminanIdMismatchException("ID tidak sesuai");
        }

        // Lakukan pembaruan data
        existingPinjaman.setJumlahPinjaman(request.getJumlahPinjaman());
        existingPinjaman.setKeperluan(request.getKeperluan());
        existingPinjaman.setLamaAngsuran(request.getLamaAngsuran());
        existingPinjaman.setBunga(request.getBunga());

        // Perbarui entitas Anggota dan Jaminan
        Anggota anggota = anggotaRepository.findById(request.getAnggotaId())
                .orElseThrow(() -> new AnggotaNotFoundException("Anggota dengan ID " + request.getAnggotaId() + " tidak ditemukan"));
        existingPinjaman.setAnggota(anggota);

        Jaminan jaminan = jaminanRepository.findById(request.getJaminanId())
                .orElseThrow(() -> new JaminanNotFoundException("Jaminan dengan ID " + request.getJaminanId() + " tidak ditemukan"));
        existingPinjaman.setJaminan(jaminan);

        // Simpan perubahan ke dalam repository
        pinjamanRepository.save(existingPinjaman);

        // Buat dan kembalikan respons
        return PinjamanRespone.builder()
                .anggotaId(existingPinjaman.getAnggota().getId())
                .jaminanId(existingPinjaman.getJaminan().getId())
                .kodeTransaksi(UUID.randomUUID().toString().substring(0, 5).toUpperCase())
                .keperluan(existingPinjaman.getKeperluan())
                .lamaAngsuran(existingPinjaman.getLamaAngsuran())
                .jumlahPinjaman(existingPinjaman.getJumlahPinjaman())
                .bunga(existingPinjaman.getBunga())
                .tanggalTransaksi(new Date())
                .build();
    }





    public String deletePinjamanById(String token, Integer id) throws PinjamanDeletionException {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
            }

            Optional<Pinjaman> pinjaman = pinjamanRepository.findById(id);

            pinjaman.ifPresent(j -> pinjamanRepository.deleteById(id));

            return pinjaman.map(j -> "Deleted Success").orElse("Pinjaman not found");
        } catch (TokenInvalidException e) {
            throw new PinjamanDeletionException("Token tidak valid", e);
        } catch (Exception e) {
            throw new PinjamanDeletionException("Error deleting Pinjaman", e);
        }
    }





}
