package com.koperasi.service;

import com.koperasi.contants.*;
import com.koperasi.dto.simpanan.SimpananRequest;
import com.koperasi.dto.simpanan.SimpananRespone;
import com.koperasi.entitiy.Anggota;
import com.koperasi.entitiy.Simpanan;
import com.koperasi.exception.anggota.AnggotaNotFoundException;
import com.koperasi.exception.simpanan.*;
import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.repository.AnggotaRepository;
import com.koperasi.repository.SimpananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SimpananService {

    @Autowired
    private SimpananRepository simpananRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AnggotaRepository anggotaRepository;


    public List<SimpananRespone> getAllSimpanan(String token) {
        if (!tokenService.getToken(token)) {
            return Collections.emptyList();
        }

        List<SimpananRespone> listSimpanan = new ArrayList<>();
        for (Simpanan simpanan : simpananRepository.findAll()) {
            listSimpanan.add(SimpananRespone.builder()
                    .anggotaId(simpanan.getAnggota().getId())
                    .jumlahSimpanan(simpanan.getJumlahSimpanan())
                    .tanggalSimpanan(new Date())
                    .build());
        }
        return listSimpanan;
    }



    public SimpananRespone saveSimpanan(String token, SimpananRequest request) {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
            }

            Optional<Anggota> optionalAnggota = anggotaRepository.findById(request.getAnggotaId());
            if (!optionalAnggota.isPresent()) {
                throw new AnggotaNotFoundException("Anggota dengan ID " + request.getAnggotaId() + ConstansService.TIDAK_DITEMUKAN_MESSAGE);
            }

            Anggota anggota = optionalAnggota.get();
            Simpanan simpanan = new Simpanan();

            simpanan.setJumlahSimpanan(request.getJumlahSimpanan());
            simpanan.setTanggalSimpanan(new Date());
            simpanan.setAnggota(anggota);
            simpananRepository.save(simpanan);

            return SimpananRespone.builder()
                    .anggotaId(simpanan.getAnggota().getId())
                    .jumlahSimpanan(simpanan.getJumlahSimpanan())
                    .tanggalSimpanan(new Date())
                    .build();
        } catch (TokenInvalidException | AnggotaNotFoundException e) {
            throw new SimpananSaveException("Gagal menyimpan Simpanan", e);
        }
    }


    public SimpananRespone updateSimpananById(String token, Integer id, SimpananRequest request) throws TokenInvalidException {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
            }

            Optional<Simpanan> optionalSimpanan = simpananRepository.findById(id);

            if (optionalSimpanan.isEmpty()) {
                throw new SimpananNotFoundException("Simpanan dengan ID " + id + " tidak ditemukan");
            }

            Simpanan existingSimpanan = optionalSimpanan.get();

            Optional<Anggota> optionalAnggota = anggotaRepository.findById(request.getAnggotaId());

            if (optionalAnggota.isEmpty()) {
                throw new AnggotaNotFoundException("Anggota dengan ID " + request.getAnggotaId() + " tidak ditemukan");
            }

            Anggota anggota = optionalAnggota.get();

            if (!existingSimpanan.getId().equals(id)) {
                throw new SimpananIdMismatchException("ID Simpanan tidak sesuai");
            } else {
                existingSimpanan.setId(id);
                existingSimpanan.setJumlahSimpanan(request.getJumlahSimpanan());
                existingSimpanan.setTanggalSimpanan(new Date());
                existingSimpanan.setAnggota(anggota);
                simpananRepository.save(existingSimpanan);
            }

            return SimpananRespone.builder()
                    .anggotaId(existingSimpanan.getAnggota().getId())
                    .jumlahSimpanan(existingSimpanan.getJumlahSimpanan())
                    .tanggalSimpanan(new Date())
                    .build();
        } catch (TokenInvalidException | SimpananNotFoundException | AnggotaNotFoundException | SimpananIdMismatchException e) {
            throw new SimpananUpdateException("Error while updating Simpanan");
        }
    }


    public String deleteSimpananById(String token, Integer id) throws TokenInvalidException {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
            }

            Optional<Simpanan> simpanan = simpananRepository.findById(id);

            if (simpanan.isPresent()) {
                simpananRepository.deleteById(id);
                return "Deleted Success";
            } else {
                return "Simpanan not found";
            }
        } catch (TokenInvalidException e) {
            throw new TokenInvalidException("Token tidak valid", e);
        } catch (Exception e) {
            throw new SimpananDeletionException("Error deleting Simpanan", e);
        }
    }



}
