package com.koperasi.service;

import com.koperasi.exception.anggota.AnggotaNotExistsException;
import com.koperasi.exception.anggota.AnggotaDeletionException;
import com.koperasi.exception.anggota.DataRetrievalException;
import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.contants.*;
import com.koperasi.dto.anggota.AnggotaRequest;
import com.koperasi.dto.anggota.AnggotaRespone;
import com.koperasi.entitiy.*;
import com.koperasi.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AnggotaService {

    @Autowired
    private AnggotaRepository anggotaRepository;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private LoginRepository loginRepository;
    @Autowired
    private JaminanRepository jaminanRepository;
    @Autowired
    private PinjamanRepository pinjamanRepository;
    @Autowired
    private SimpananRepository simpananRepository;



    public List<AnggotaRespone> getAllAnggota(String token) throws TokenInvalidException {
        if (!tokenService.getToken(token)) {
            throw new TokenInvalidException("Token tidak valid");
        }

        List<AnggotaRespone> listAnggota = new ArrayList<>();
        try {
            for (Anggota anggota : anggotaRepository.findAll()) {
                listAnggota.add(AnggotaRespone.builder()
                        .id(anggota.getId())
                        .nama(anggota.getNama())
                        .alamat(anggota.getAlamat())
                        .noTelepon(anggota.getNoTelepon())
                        .email(anggota.getEmail())
                        .tanggalLahir(anggota.getTanggalLahir())
                        .jenisKelamin(anggota.getJenisKelamin())
                        .pekerjaan(anggota.getPekerjaan())
                        .build());
            }
        } catch (Exception e) {
            throw new DataRetrievalException("Error saat mengambil data anggota", e);
        }

        return listAnggota;
    }



    public AnggotaRespone saveAnggota(AnggotaRequest request){
        Anggota anggota = new Anggota();

        anggota.setNama(request.getNama());
        anggota.setAlamat(request.getAlamat());
        anggota.setNoTelepon(request.getNoTelepon());
        anggota.setEmail(request.getEmail());
        anggota.setTanggalLahir(request.getTanggalLahir());
        anggota.setJenisKelamin(request.getJenisKelamin());
        anggota.setPekerjaan(request.getPekerjaan());
        anggotaRepository.save(anggota);

        return AnggotaRespone.builder()
                .id(anggota.getId())
                .nama(anggota.getNama())
                .alamat(anggota.getAlamat())
                .noTelepon(anggota.getNoTelepon())
                .email(anggota.getEmail())
                .tanggalLahir(anggota.getTanggalLahir())
                .jenisKelamin(anggota.getJenisKelamin())
                .pekerjaan(anggota.getPekerjaan())
                .build();
    }


    public AnggotaRespone updateAnggotaById(String token, Integer id, AnggotaRequest request) throws TokenInvalidException {
        if (!tokenService.getToken(token)) {
            throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
        }

        Optional<Anggota> anggota = anggotaRepository.findById(id);

        if (anggota.isPresent()) {
            Anggota a = new Anggota();
            a.setId(id);
            a.setNama(request.getNama());
            a.setAlamat(request.getAlamat());
            a.setNoTelepon(request.getNoTelepon());
            a.setEmail(request.getEmail());
            a.setTanggalLahir(request.getTanggalLahir());
            a.setJenisKelamin(request.getJenisKelamin());
            a.setPekerjaan(request.getPekerjaan());
            anggotaRepository.save(a);

            return AnggotaRespone.builder()
                    .id(a.getId())
                    .nama(request.getNama())
                    .alamat(request.getAlamat())
                    .noTelepon(request.getNoTelepon())
                    .email(request.getEmail())
                    .tanggalLahir(request.getTanggalLahir())
                    .jenisKelamin(request.getJenisKelamin())
                    .pekerjaan(request.getPekerjaan())
                    .build();
        } else {
            throw new AnggotaNotExistsException("Anggota dengan ID " + id + " Tidak ada.");
        }
    }


    public String deleteAnggotaById(String token, Integer id) throws AnggotaDeletionException {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
            }

            Optional<Anggota> anggota = anggotaRepository.findById(id);

            if (anggota.isPresent()) {
                List<Pinjaman> relatedPinjaman = pinjamanRepository.findByAnggotaId(id);
                pinjamanRepository.deleteAll(relatedPinjaman);

                List<Login> relatedLogins = loginRepository.findByAnggotaId(id);
                loginRepository.deleteAll(relatedLogins);

                List<Jaminan> relatedJaminan = jaminanRepository.findByAnggotaId(id);
                jaminanRepository.deleteAll(relatedJaminan);

                List<Simpanan> relatedSimpanan = simpananRepository.findByAnggotaId(id);
                simpananRepository.deleteAll(relatedSimpanan);

                anggotaRepository.deleteById(id);
                return "Berhasil Dihapus";
            } else {
                return "Anggota tidak ditemukan";
            }
        } catch (TokenInvalidException e) {
            throw new AnggotaDeletionException("Token tidak valid", e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AnggotaDeletionException("Error saat menghapus Anggota", e);
        }
    }





}
