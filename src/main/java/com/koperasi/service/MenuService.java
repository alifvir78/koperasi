package com.koperasi.service;


import com.koperasi.exception.menu.MenuNotFoundException;
import com.koperasi.dto.menu.MenuRequest;
import com.koperasi.dto.menu.MenuRespon;
import com.koperasi.entitiy.Menu;
import com.koperasi.repository.MenuRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {

    @Autowired
    private MenuRepository menuRepository;

    public List<Menu> getAllMenu() {
        List<Menu> menuList = menuRepository.findAll();
        return List.copyOf(menuList);
    }



    //ini menggunakan beans util, untuk req dan respone
    public MenuRespon createMenu(MenuRequest request){
        //set req
        Menu menu = new Menu();
        BeanUtils.copyProperties(request, menu);
        //save
        Menu savedMmenu = menuRepository.save(menu);
        //return/respone
        MenuRespon response = new MenuRespon();
        BeanUtils.copyProperties(savedMmenu, response);

        return response;
        }



    public MenuRespon updateMenuById(Integer id, MenuRequest request) {
        Menu menu = menuRepository.findById(id)
                .orElseThrow(() -> new MenuNotFoundException("Menu dengan ID " + id + " tidak ditemukan"));

        // Update properti yang sesuai
        menu.setNameMenu(request.getNameMenu());
        menu.setPrice(request.getPrice());
        menu.setDescription(request.getDescription());
        menu.setUrlImage(request.getUrlImage());

        // Simpan perubahan ke database
        Menu savedMenu = menuRepository.save(menu);

        // Buat respons
        MenuRespon response = new MenuRespon();
        BeanUtils.copyProperties(savedMenu, response);

        return response;
    }


    }



