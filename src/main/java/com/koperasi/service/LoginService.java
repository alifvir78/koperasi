package com.koperasi.service;

import com.koperasi.dto.login.LoginRequest;
import com.koperasi.dto.login.LoginRespone;
import com.koperasi.entitiy.Anggota;
import com.koperasi.entitiy.Login;
import com.koperasi.repository.AnggotaRepository;
import com.koperasi.repository.LoginRepository;
import com.koperasi.util.JwtToken;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class LoginService {

    @Autowired
    private AnggotaRepository anggotaRepository;

    @Autowired
    private LoginRepository loginRepository;

    public LoginRespone login(@NotNull LoginRequest request){
        Optional<Anggota> optionalAnggota = anggotaRepository.findWithUserDataByEmailAndNoTelepon(request.getNoTelepon(), request.getEmail());
        if(!optionalAnggota.isPresent()){
            return null;
        }
        Login login = new Login();
        Anggota anggota = optionalAnggota.get();

        anggota.setId(anggota.getId());
        anggota.setNoTelepon(request.getNoTelepon());
        anggota.setEmail(request.getEmail());
        login.setToken(JwtToken.getToken(request));
        login.setAnggota(anggota);
        loginRepository.save(login);

        return LoginRespone.builder()
                .token(login.getToken())
                .build();
    }



}
