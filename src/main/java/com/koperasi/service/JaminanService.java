package com.koperasi.service;

import com.koperasi.contants.*;
import com.koperasi.dto.jaminan.JaminanRequest;
import com.koperasi.dto.jaminan.JaminanRespone;
import com.koperasi.entitiy.Anggota;
import com.koperasi.entitiy.Jaminan;
import com.koperasi.entitiy.Pinjaman;
import com.koperasi.exception.anggota.AnggotaNotFoundException;
import com.koperasi.exception.jaminan.*;
import com.koperasi.exception.token.TokenInvalidException;
import com.koperasi.repository.AnggotaRepository;
import com.koperasi.repository.JaminanRepository;
import com.koperasi.repository.PinjamanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class JaminanService {

    @Autowired
    private JaminanRepository jaminanRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AnggotaRepository anggotaRepository;

    @Autowired
    private PinjamanRepository pinjamanRepository;


    public List<JaminanRespone> getAllJaminan(String token) throws TokenInvalidException {
        if (!tokenService.getToken(token)) {
            throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
        }


        List<JaminanRespone> listJaminan = new ArrayList<>();
        for (Jaminan jaminan : jaminanRepository.findAll()) {
            listJaminan.add(JaminanRespone.builder()
                    .anggotaId(jaminan.getAnggota().getId())
                    .detailJaminan(jaminan.getDetailJaminan())
                    .nominalJaminan(jaminan.getNominalJaminan())
                    .tanggalJaminan(new Date())
                    .build());
        }
        return listJaminan;
    }


    public JaminanRespone saveJaminan(String token, JaminanRequest request) {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
            }

            Optional<Anggota> optionalAnggota = anggotaRepository.findById(request.getAnggotaId());
            if (!optionalAnggota.isPresent()) {
                throw new AnggotaNotFoundException("Anggota dengan ID " + request.getAnggotaId() + ConstansService.TIDAK_DITEMUKAN_MESSAGE);
            }
            Anggota anggota = optionalAnggota.get();

            Jaminan jaminan = new Jaminan();
            jaminan.setDetailJaminan(request.getDetailJaminan());
            jaminan.setNominalJaminan(request.getNominalJaminan());
            jaminan.setAnggota(anggota);
            jaminan.setTanggalJaminan(new Date());
            jaminanRepository.save(jaminan);

            return JaminanRespone.builder()
                    .anggotaId(jaminan.getAnggota().getId())
                    .detailJaminan(jaminan.getDetailJaminan())
                    .nominalJaminan(jaminan.getNominalJaminan())
                    .tanggalJaminan(new Date())
                    .build();
        } catch (TokenInvalidException | AnggotaNotFoundException e) {
            e.printStackTrace();
            throw new JaminanSaveException("Failed to save Jaminan", e);

        }
    }

    public JaminanRespone updateJaminanById(String token, Integer id, JaminanRequest request) {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException(ConstansService.TOKEN_INVALID_MESSAGE);
            }

            Optional<Jaminan> optionalJaminan = jaminanRepository.findById(id);
            if (!optionalJaminan.isPresent()) {
                throw new JaminanNotFoundException("Jaminan dengan ID " + id + " tidak ditemukan");
            }
            Jaminan jaminan = optionalJaminan.get();

            Optional<Anggota> optionalAnggota = anggotaRepository.findById(request.getAnggotaId());
            if (!optionalAnggota.isPresent()) {
                throw new AnggotaNotFoundException("Anggota dengan ID " + request.getAnggotaId() + " tidak ditemukan");
            }
            Anggota anggota = optionalAnggota.get();

            if (!jaminan.getId().equals(id)) {
                throw new JaminanIdMismatchException("ID jaminan tidak sesuai");
            } else {
                jaminan.setDetailJaminan(request.getDetailJaminan());
                jaminan.setNominalJaminan(request.getNominalJaminan());
                jaminan.setAnggota(anggota);
                jaminan.setTanggalJaminan(new Date());
                jaminanRepository.save(jaminan);
            }

            return JaminanRespone.builder()
                    .anggotaId(jaminan.getAnggota().getId())
                    .detailJaminan(jaminan.getDetailJaminan())
                    .nominalJaminan(jaminan.getNominalJaminan())
                    .tanggalJaminan(new Date())
                    .build();
        } catch (TokenInvalidException | JaminanNotFoundException | AnggotaNotFoundException | JaminanIdMismatchException e) {
            e.printStackTrace();
            throw new JaminanUpdateException("Failed to update Jaminan", e);
        }
    }



    public String deleteJaminanById(String token, Integer id) throws JaminanDeletionException {
        try {
            if (!tokenService.getToken(token)) {
                throw new TokenInvalidException("Token tidak valid");
            }

            Optional<Jaminan> optionalJaminan = jaminanRepository.findById(id);

            optionalJaminan.ifPresent(j -> {
                List<Pinjaman> relatedPinjaman = pinjamanRepository.findByJaminanId(id);
                pinjamanRepository.deleteAll(relatedPinjaman);

                jaminanRepository.deleteById(id);
            });

            return optionalJaminan.map(j -> "Deleted Success").orElse("Jaminan not found");
        } catch (TokenInvalidException e) {
            throw new JaminanDeletionException("Token tidak valid", e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new JaminanDeletionException("Error deleting Jaminan", e);
        }
    }




}
