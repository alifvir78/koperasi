package com.koperasi.entitiy;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Simpanan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double  jumlahSimpanan;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date tanggalSimpanan;

    @OneToOne
    private Anggota anggota;

}
