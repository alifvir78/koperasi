package com.koperasi.entitiy;


import lombok.Data;

import javax.persistence.*;


@Entity
@Data
public class Login {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;
    private String token; //image


    @OneToOne
    private Anggota anggota;
}
