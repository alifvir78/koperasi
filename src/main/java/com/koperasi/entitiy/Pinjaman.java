package com.koperasi.entitiy;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
public class Pinjaman {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date tanggalTransaksi;
    private Double jumlahPinjaman;
    private String keperluan;
    private String lamaAngsuran;
    private BigDecimal bunga;
    private String kodeTransaksi;

    @ManyToOne
    private Anggota anggota;

    @ManyToOne
    private Jaminan jaminan;



}
