package com.koperasi.entitiy;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Jaminan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String detailJaminan;
    private Double nominalJaminan;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date tanggalJaminan;


    @ManyToOne
    private Anggota anggota;

}
